
public class ColorSort {

    enum Color {red, green, blue}

    public static void main(String[] param) {
        // for debugging
    }

    public static void reorder(Color[] balls) {
        int[] countArray = new int[3];
        int charIndex;

        // Store count for each ball
        for (int i = 0; i < balls.length; i++) {

            if (balls[i] == Color.red) {  // Red balls on index 0
                charIndex = 0;
            } else if (balls[i] == Color.green) { // Green balls on index 1
                charIndex = 1;
            } else { // Blue balls on index 2
                charIndex = 2;
            }
            countArray[charIndex]++;
        }

        // Replace numbers with balls
        int j = 0;
        for (int i = 0; i < countArray.length; i++) {

            while (countArray[i] > 0) {

                if (i == 0) {
                    balls[j] = Color.red;
                }
                else if (i == 1) {
                    balls[j] = Color.green;
                }
                else{
                    balls[j] = Color.blue;
                }
                --countArray[i]; // Decrement the ball count by one
                ++j; // Go to the next index in the final array
            }
        }
    }
}







